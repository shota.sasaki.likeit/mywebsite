package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class UserupdateServlet
 */
@WebServlet("/UserupdateServlet")
public class UserupdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserupdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 request.setCharacterEncoding("UTF-8");

			//jspから送られてきたloginIdのデータを取得
			//getParaの中身はbeansの名前
			String id= request.getParameter("id");

			// 確認用：idをコンソールに出力
			System.out.println(id);

			UserDao userDao = new UserDao();
			User user = userDao. findid(id);



			request.setAttribute("user",user);


			// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);





	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得

		String id= request.getParameter("id");
		String loginId = request.getParameter("loginId");

		String password = request.getParameter("password");
		String passwordConf = request.getParameter("passwordConf");
		String userName=request.getParameter("userName");
		String birthDate=request.getParameter("birthDate");

		UserDao userDao = new UserDao();

		if(!(password.equals(passwordConf))) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			User user = userDao. findid(id);
			request.setAttribute("user",user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
			return;
		}else if(password.isEmpty()||passwordConf.isEmpty()||userName.isEmpty()||birthDate.isEmpty()) {

			request.setAttribute("errMsg", "入力された内容は正しくありません");
			User user = userDao. findid(id);
			request.setAttribute("user",user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
			return;
		}



		userDao.updateuser(loginId,password,passwordConf,userName,birthDate);

		User user = userDao.findid(id);
		request.setAttribute("user",user);

		User userinfo = userDao.findByLoginInfo(loginId, password);
		HttpSession session = request.getSession();
		session.setAttribute("userInfo", userinfo);




		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdetail.jsp");
		dispatcher.forward(request, response);


	}


	}


