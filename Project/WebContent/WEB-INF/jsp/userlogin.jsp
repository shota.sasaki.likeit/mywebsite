<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <link href="css/common.css" rel="stylesheet" type="text/css" />


</head>

<body>
    <div id="wrapper">

        <header class="navbar  navbar-light " style="background-color: azure" ;>

            <h1>
                <p class="fishtitle text-primary">熱帯魚の館</p>
            </h1>

        </header>
        <br>
        <br>

        <div class="text-center col-sm-9">
            <big>ログイン画面</big>
        </div>

        <c:if test="${errMsg != null}">
            <div class="alert alert-danger" role="alert">${errMsg}</div>
        </c:if>
        <br>
        <form class="login" action="LoginServlet" method="post">
            <div class="form-group row">
                <label for="inputid" class="col-sm-2 col-form-label"> 　　ログインID</label>
                <div class="col-sm-5">
                    <input name="loginId" type="id" class="form-control" id="inputid">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">
                    　　パスワード</label>
                <div class="col-sm-5">
                    <input name="password" type="password" class="form-control" id="inputPassword">
                </div>
            </div>
            <br>

            <button type="submit" value="login" class="loginbutton col-sm-2""btnbtn-primarybtn-lgbtn-block">ログイン</button>
        </form>
        <br>



        <footer>
            <ul><li><a href="userresisterServlet"> ユーザ情報新規登録</a></li></ul>
        </footer>
    </div>
</body></html>