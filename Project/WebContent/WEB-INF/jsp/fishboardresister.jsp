<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>title</title>

    <link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">

<link href="css/common.css" rel="stylesheet" type="text/css" />
    </head>

<body>
<div id="wrapper">

  <nav class="navbar  navbar-light " style="background-color: azure";>

             <h1><p class="fishtitle text-primary">熱帯魚の館</p>
         </h1>
         <ul class="l navbar-nav flex-row">
            <li class="nav-item">
                    <a class="nav-link"  href="userdetailservlet?id=${userInfo.id}"> ${userInfo.name}さん</a>
               </li>
            <li class="nav-item">
                     　　 <a class="btn btn-primary" href="LoginServlet">ログアウト</a>
               </li>
           </ul>
    </nav>

    <br>
    <h2>　熱帯魚掲示板の登録</h2>

    <form class="form-update" action="FishboardresisterServlet?fish_id=${fishdetail.fishid}&userid=${userInfo.id}" method="post">
  　
        <div class="form-group row">
        <label for="boardtitle" class="col-sm-2 col-form-label" >　　　掲示板の名前</label>
        <div class="col-sm-5">
            <input type="text" class="form-control" id="boardname" name="boardname">
        </div>
    </div>

    <div class="form-group row">
        <label for="boardtext" class="col-sm-2 col-form-label" >　　　本文</label>
        <div class="col-sm-5">
        <textarea rows="7" cols="70" name="boardtext"></textarea>
        </div>
    </div>



    <div class="col s6 center-align">
     　　<button type="submit" value="fishresister" class="fishresisterbutton col-sm-2""btn btn-primary btn-lg btn-block">登録</button>
    </div>
<a class="nav-link" href="FishdetailServlet?fish_id=${fishdetail.fishid}"> 戻る</a>
    </form>
  <footer>
            <a href="fishresister?userid=${userInfo.id}">　　熱帯魚情報登録</a>
        </footer>
    </div>
</body></html>