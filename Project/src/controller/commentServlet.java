package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comment;
import beans.Fish;
import dao.CommentDao;
import dao.FishboardDao;

/**
 * Servlet implementation class commentServlet
 */
@WebServlet("/commentServlet")
public class commentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public commentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  request.setCharacterEncoding("UTF-8");

			String comment= request.getParameter("comment");
			String userid=request.getParameter("userid");
			String boardid=request.getParameter("boardid");

			int boardid2=Integer.parseInt(boardid);
			int userid2=Integer.parseInt(userid);

			CommentDao commentdao=new CommentDao();
			FishboardDao Fishboarddao=new FishboardDao();

			try {
				int i=commentdao.commentresister(userid2, boardid2, comment);


				ArrayList<Comment> commentList=commentdao.commentList(boardid2);

				request.setAttribute("commentList", commentList);

				Fish fishboarddetail=Fishboarddao.fishboarddetail(boardid2);
				request.setAttribute("fishboarddetail", fishboarddetail);

			} catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
				System.out.println(123);
			}





			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/fishboard.jsp");
			dispatcher.forward(request, response);





	}

}
