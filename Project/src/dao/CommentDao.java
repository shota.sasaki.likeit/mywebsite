package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.Comment;




public class CommentDao {
	public int commentresister(int userid,int boardid,String comment) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt=null;
		int i=1;//登録に成功したら1を返す

		try {


			conn = DBManager.getConnection();
			String sql="INSERT INTO comment(user_id,boards_id,create_date,text)VALUES(?,?,NOW(),?)";

			stmt  =  conn.prepareStatement(sql);

			stmt.setInt(1,userid);
			stmt.setInt(2,boardid);
			stmt.setString(3, comment);

	        stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return i;
	}

	public ArrayList<Comment> commentList(int boardid) throws SQLException {
		Connection con=null;
		PreparedStatement st=null;

		try {
			con = DBManager.getConnection();

			st=con.prepareStatement("SELECT user.name,comment.id,comment.text  FROM comment INNER JOIN user"
					+ " ON comment.user_id = user.id WHERE comment.boards_id=?");
			st.setInt(1, boardid);

			ResultSet rs=st.executeQuery();
			ArrayList<Comment> commentList=new ArrayList<Comment>();

			while(rs.next()) {
				Comment cm=new Comment();
				cm.setName(rs.getString("name"));
				cm.setId(rs.getInt("id"));
				cm.setText(rs.getString("text"));

				commentList.add(cm);
			}

			return commentList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public void commentdelete(int commentid) {
		Connection con=null;
		PreparedStatement st=null;

		try {
			con=DBManager.getConnection();
			String sql="DELETE FROM comment WHERE id =? ";

			st = con.prepareStatement(sql);

			st.setInt(1, commentid);

			st.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		}
	}
}