<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>title</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="css/common.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <div id="wrapper">
        <nav class="navbar  navbar-light " style="background-color: azure" ;>

            <h1>
                <p class="fishtitle text-primary">熱帯魚の館</p>
            </h1>
            <ul class="l navbar-nav flex-row">
                <li class="nav-item">
                    <a class="nav-link" href="userdetailservlet?id=${userInfo.id}"> ${userInfo.name}さん</a>
                </li>
                <li class="nav-item">
                    　　 <a class="btn btn-primary" href="LoginServlet">ログアウト</a>
                </li>
            </ul>
        </nav>



        <div class="container">
            <div class="row center">

                <form action="Fishsearchresultservlet" method="get">
                    <i class="material-icons prefix">熱帯魚の名前検索</i> <input type="text" name="search_word" value="">
                    <button type="submit" class="btn btn-primary">検索</button>
                </form>
            </div>







        <c:if test="${searchWord != null}">
            <a href="ItemSearchResult?search_word=${searchWord}&page_num=${pageNum}" class="btn waves-effect waves-light">検索結果へ </a>
        </c:if>




        <br>
        <div class="row">
            <c:forEach var="fishviewList" items="${fishviewList}" varStatus="status"> <%--statusは画像の枚数--%>
                <div class="col s12 m3">
                    <div class="card">
                        <div class="card-image">
                            <a href="FishdetailServlet?fish_id=${fishviewList.fishid}&page_num=${pageNum}"><img src="image/${fishviewList.fishImageName}" class="fishview"></a>
                        </div>
                        <div class="card-content">
                            <span class="card-title">${fishviewList.fishName}</span>
                        </div>
                    </div>
                </div>
                <c:if test="${(status.index + 1) % 4 == 0}">
        </div>
        <div class="row">
            </c:if>
            </c:forEach>
        </div>

</div>
        <br>



        <footer >
        <ul>
         <li><a  href="userdetailservlet?id=${userInfo.id}"> ${userInfo.name}さんのユーザ情報</a></li>
         <li> <a href="fishresister?userid=${userInfo.id}">　　熱帯魚情報登録</a></li>
            </ul>
        </footer>
    </div>
</body>

</html>
