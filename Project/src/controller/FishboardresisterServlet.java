package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Fish;
import dao.FishDao;
import dao.FishboardDao;

/**
 * Servlet implementation class FishboardresisterServlet
 */
@WebServlet("/FishboardresisterServlet")
public class FishboardresisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public FishboardresisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 request.setCharacterEncoding("UTF-8");

			//jspから送られてきたfishidのデータを取得
			//getParaの中身はbeansの名前

			String fishid =request.getParameter("fish_id");
			int fishid2=Integer.parseInt(fishid);  //getParameterメソッドはString型でしか取得できないため、数値が送られてきた場合はintに変換する。


			FishDao fishdao=new FishDao();


			Fish fishdetail;
			try {
				fishdetail = fishdao.fishdetailfindbyfishid(fishid2);
				request.setAttribute("fishdetail", fishdetail);
			} catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}




		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/fishboardresister.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		 request.setCharacterEncoding("UTF-8");
		 String boardname=request.getParameter("boardname");
		 String boardtext=request.getParameter("boardtext");
		 String fishid=request.getParameter("fish_id");
		 String userid=request.getParameter("userid");

			int fishid2=Integer.parseInt(fishid);
			int userid2=Integer.parseInt(userid);


		 FishboardDao Fishboarddao=new FishboardDao();


		 try {
			Fishboarddao.insertfishboard(boardname,boardtext,fishid2,userid2);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		 response.sendRedirect(request.getContextPath()+"/FishdetailServlet?fish_id="+fishid2);





	}

}
