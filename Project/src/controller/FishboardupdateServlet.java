package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Fish;
import dao.FishboardDao;

/**
 * Servlet implementation class FishboardupdateServlet
 */
@WebServlet("/FishboardupdateServlet")
public class FishboardupdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public FishboardupdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String fishboardid =request.getParameter("fishboardid");
		int fishboardid2=Integer.parseInt(fishboardid);
		FishboardDao Fishboarddao=new FishboardDao();

		Fish fishboarddetail;
		try {
			fishboarddetail = Fishboarddao.fishboarddetail(fishboardid2);
			request.setAttribute("fishboarddetail", fishboarddetail);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/fishboardupdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 request.setCharacterEncoding("UTF-8");
		String fishboardid =request.getParameter("fishboardid");
		String userid=request.getParameter("userid");
		String boardname=request.getParameter("boardname");
		String text=request.getParameter("text");

		int fishboardid2=Integer.parseInt(fishboardid);
		int userid2=Integer.parseInt(userid);

		FishboardDao fishboarddao=new FishboardDao();
		try {
			fishboarddao.fishboardupdate(fishboardid2,boardname,text);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		 response.sendRedirect(request.getContextPath()+"/FishboardServlet?fishboardid="+fishboardid2);

	}

}
