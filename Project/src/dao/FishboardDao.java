package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.Fish;

public class FishboardDao {
	public void insertfishboard(String boardname, String boardtext,int fishid,int userid) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt=null;

		try {


			conn = DBManager.getConnection();
			String sql="INSERT INTO boards(user_id,fish_id,title_name,create_date,text)VALUES(?,?,?,NOW(),?)";

			stmt  =  conn.prepareStatement(sql);

			stmt.setInt(1,userid);
			stmt.setInt(2,fishid);
			stmt.setString(3, boardname);
			stmt.setString(4, boardtext);

	        stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}



	public ArrayList<Fish> fishboardListbyfishid(int fishid2) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT id,user_id,fish_id,title_name,text FROM boards WHERE fish_id=? ");


			st.setInt(1, fishid2);

			ResultSet rs=st.executeQuery();

			ArrayList<Fish> fishboardList=new ArrayList<Fish> ();

			while(rs.next()) {

				int boardid=rs.getInt("id");
				int userid=rs.getInt("user_id");
				int fishid=rs.getInt("fish_id");
				String boardname=rs.getString("title_name");
				String text=rs.getString("text");

				Fish fishboardview=new Fish(boardid,userid,fishid,boardname,text);
				fishboardList.add(fishboardview);
			}

			System.out.println("getAllItem completed");
			return fishboardList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


 public Fish fishboarddetail(int fishboardid) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT user_id,fish_id,title_name,text,create_date FROM boards WHERE id=? ");
			st.setInt(1,fishboardid);

			ResultSet rs=st.executeQuery();
			if (!rs.next()) {
				return null;
			}

			int fishid=rs.getInt("fish_id");
			int userid= rs.getInt("user_id");
			String boardname=rs.getString("title_name");
			String text= rs.getString("text");
			Date createdate=rs.getDate("create_date");


	        Fish Fishboarddetail=new Fish(fishid,fishboardid,userid,boardname,text,createdate);

		return Fishboarddetail;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
		// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}



public void fishboarddelete(int fishboardid) throws SQLException {
	Connection conn=null;
	PreparedStatement stmt=null;

	try {
		conn=DBManager.getConnection();
		String sql="DELETE FROM boards WHERE id =? ";

		stmt = conn.prepareStatement(sql);

		stmt.setInt(1, fishboardid);

		stmt.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}
}



public void fishboardupdate(int fishboardid,String boardname,String text) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt=null;

		try {

			// データベースへ接続
			conn = DBManager.getConnection();
			String sql = "UPDATE boards SET title_name=?,text=?,update_date=NOW() ";

			sql+="WHERE id=?";

			stmt  =  conn.prepareStatement(sql);

			stmt.setString(1, boardname);
			stmt.setString(2, text);
			stmt.setInt(3, fishboardid);



			stmt.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}





