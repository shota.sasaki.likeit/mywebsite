package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import beans.Fish;
import dao.FishDao;

/**
 * Servlet implementation class FishupdateServlet
 */
@WebServlet("/FishupdateServlet")
@MultipartConfig(location="C:\\Users\\LIKEIT_STUDENT.DESKTOP-R210GVO\\Documents\\mywebsite\\Project\\WebContent\\image", maxFileSize=1048576)
public class FishupdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public FishupdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String fishid= request.getParameter("fishid");
		int fishid2=Integer.parseInt(fishid);


		FishDao fishDao = new FishDao();
		Fish fishdetail;
		try {
			fishdetail = fishDao.fishdetailfindbyfishid(fishid2);
			request.setAttribute("fishdetail",fishdetail);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}






		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/fishupdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得

		String fishid= request.getParameter("fishid");
		int fishid2=Integer.parseInt(fishid);

		String fishName = request.getParameter("fishName");

		String fishhabitat = request.getParameter("fishhabitat");
		String fishsize = request.getParameter("fishsize");
		String fishlifespan=request.getParameter("fishlifespan");
		String Fishdetail=request.getParameter("fishdetail");

		 Part image =  request.getPart("fishimage");
		 String fishImageName = this.getFileName(image);

		FishDao fishDao = new FishDao();


		if(fishName.isEmpty()||fishhabitat.isEmpty()||fishlifespan.isEmpty()||Fishdetail.isEmpty()) {

			request.setAttribute("errMsg", "入力された内容は正しくありません");

			Fish fishdetail;
			try {
				fishdetail = fishDao.fishdetailfindbyfishid(fishid2);
				request.setAttribute("fishdetail",fishdetail);
			} catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}


			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/fishupdate.jsp");
			dispatcher.forward(request, response);
			return;
		}



		try {
			fishDao.updatefish(fishImageName,fishid2,fishName,fishhabitat,fishsize,fishlifespan,Fishdetail);
			Fish fishdetail = fishDao.fishdetailfindbyfishid(fishid2);
			request.setAttribute("fishdetail",fishdetail);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}










		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/fishdetail.jsp");
		dispatcher.forward(request, response);


	}
		private String getFileName(Part part) {
	        String name = null;
	        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
	            if (dispotion.trim().startsWith("filename")) {
	                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
	                name = name.substring(name.lastIndexOf("\\") + 1);
	                break;
	            }
	        }
	        return name;
	    }
	}







