package beans;

import java.io.Serializable;
import java.sql.Date;

public class Fish implements Serializable {
		private String fishImageName;
		private String fishName;
		private int fishid;
		private int userid;
		private String fishhabitat;
		private String fishsize;
		private String fishlifespan;
        private String fishdetail;
        private int boardid;
        private String boardname;
    	private String boardtext;
    	private String text;
    	private Date createdate;




		public String getText() {
			return text;
		}


		public void setText(String text) {
			this.text = text;
		}


		public String getBoardtext() {
			return boardtext;
		}


		public void setBoardtext(String boardtext) {
			this.boardtext = boardtext;
		}


		public String getBoardname() {
			return boardname;
		}


		public void setBoardname(String boardname) {
			this.boardname = boardname;
		}





		public Fish() {

        }


		public Fish(int fishid,int userid, String fishName, String fishhabitat, String fishsize, String fishlifespan,
				String fishdetail, String fishImageName) {

			this.fishid=fishid;
			this.userid=userid;
			this.fishName=fishName;
			this.fishhabitat=fishhabitat;
			this.fishsize=fishsize;
			this.fishlifespan=fishlifespan;
			this.fishdetail=fishdetail;
			this.fishImageName=fishImageName;

		}
		public Fish(int fishid, String fishName, String fishImageName) {


			this.fishid=fishid;
			this.fishName=fishName;


			this.fishImageName=fishImageName;

		}
		public Fish(int boardid,int userid, int fishid, String boardname, String text) {

			this.boardid=boardid;
			this.userid=userid;
			this.fishid=fishid;
			this.boardname=boardname;
			this.text=text;
		}


		public Fish(int fishid,int boardid,int userid, String boardname, String text, Date createdate) {
			this.fishid=fishid;
			this.boardid=boardid;
			this.userid=userid;
			this.boardname=boardname;
			this.text=text;
			this.createdate=createdate;
		}


		public String getFishName() {
			return fishName;
		}
		public void setFishName(String fishName) {
			this.fishName = fishName;
		}
		public String getFishImageName() {
			return fishImageName;
		}
		public void setFishImageName(String fishImageName) {
			this.fishImageName = fishImageName;
		}
		public int getFishid() {
			return fishid;
		}
		public void setFishid(int fishid) {
			this.fishid=fishid;
		}
		public String getFishhabitat() {
			return fishhabitat;
		}
		public void setFishhabitat(String fishhabitat) {
			this.fishhabitat = fishhabitat;
		}
		public String getFishsize() {
			return fishsize;
		}
		public void setFishsize(String fishsize) {
			this.fishsize = fishsize;
		}
		public String getFishlifespan() {
			return fishlifespan;
		}
		public void setFishlifespan(String fishlifespan) {
			this.fishlifespan = fishlifespan;
		}
		public String getFishdetail() {
			return fishdetail;
		}
		public void setFishdetail(String fishdetail) {
			this.fishdetail = fishdetail;
		}
		public int getUserid() {
			return userid;
		}
		public void setUserid(int userid) {
			this.userid = userid;
}
	    public int getBoardid() {
				return boardid;
			}
	    public void setBoardid(int boardid) {
				this.boardid = boardid;
			}
public Date getCreatedate() {
	return createdate;
}
public void setCreatedate(Date createdate) {
	this.createdate = createdate;
}
}