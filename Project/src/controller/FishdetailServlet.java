package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Fish;
import dao.FishDao;
import dao.FishboardDao;

/**
 * Servlet implementation class FishdetailServlet
 */
@WebServlet("/FishdetailServlet")
public class FishdetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public FishdetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 request.setCharacterEncoding("UTF-8");

			//jspから送られてきたfishidのデータを取得
			//getParaの中身はbeansの名前

			String fishid =request.getParameter("fish_id");
			int fishid2=Integer.parseInt(fishid);  //getParameterメソッドはString型でしか取得できないため、数値が送られてきた場合はintに変換する。


			FishDao fishdao=new FishDao();



			try {
				Fish fishdetail = fishdao.fishdetailfindbyfishid(fishid2);
				request.setAttribute("fishdetail", fishdetail);
			} catch (SQLException e1) {
				// TODO 自動生成された catch ブロック
				e1.printStackTrace();
			}




			try {

				 FishboardDao Fishboarddao=new FishboardDao();
				ArrayList<Fish> fishboardList=Fishboarddao.fishboardListbyfishid(fishid2);

				request.setAttribute("fishboardList", fishboardList);

			} catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/fishdetail.jsp");
			dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
