<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>title</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="css/common.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <div id="wrapper">

        <nav class="navbar  navbar-light " style="background-color: azure" ;>

            <h1>
                <p class="fishtitle text-primary">熱帯魚の館</p>
            </h1>

            <ul class="l navbar-nav flex-row">
                <li class="nav-item">
                    <a class="nav-link"> ${userInfo.name}さん</a>
                </li>
                <li class="nav-item">
                    　　 <a class="btn btn-primary" href="LoginServlet">ログアウト</a>
                </li>
            </ul>

        </nav>

        <br>
        <h2>　　　熱帯魚情報登録</h2>
        <br>
        <form enctype="multipart/form-data" action="fishresister?userid=${userInfo.id}" method="post">

            <c:if test="${errMsg!= null}">
                <div class="alert alert-danger" role="alert">
                    ${errMsg}
                </div>
            </c:if>



            <div>　　①熱帯魚の画像をアップロードしてください。</div>

            <br>

            <div class="form-group">
                <label for="exampleInputFile">　　　File input</label>
                <input name="fishimage" type="file" id="InputFile">
            </div>


            　　<div>　　②熱帯魚の詳細情報を入力して下さい。</div>
            <br>
            <div class="form-group row">
                <label for="fishName" class="col-sm-2 col-form-label">　　　熱帯魚の名前</label>
                <div class="col-sm-5">
                    <input name="fishName" type="text" class="form-control" id="fishName">
                </div>
            </div>

            <div class="form-group row">
                <label for="fishhabitat" class="col-sm-2 col-form-label">　　　生息地</label>
                <div class="col-sm-5">
                    <input name="fishhabitat" type="text" class="form-control" id="fishhabitat">
                </div>
            </div>

            <div class="form-group row">
                <label for="fishsize" class="col-sm-2 col-form-label">　　　体長</label>
                <div class="col-sm-5">
                    <input name="fishsize" type="text" class="form-control" id="fishsize" value=平均、最大>
                </div>
            </div>

            <div class="form-group row">
                <label for="fishlifespan" class="col-sm-2 col-form-label">　　　寿命</label>
                <div class="col-sm-5">
                    <input name="fishlifespan" type="text" class="form-control" id="fishlifespan" value=平均、最長>
                </div>
            </div>

            <div class="form-group row">
                <label for="fishdetail" class="col-sm-2 col-form-label">　　　特徴</label>
                <div class="col-sm-5">
                    <textarea rows="7" cols="70" name="fishdetail"></textarea>
                </div>
            </div>



            <br>

            <div class="col s6 center-align">
                <button type="submit" value="fishresister" class="fishresisterbutton col-sm-4""btn btn-primary btn-lg btn-block">登録</button>
            </div>

        </form>

        　　<a class="nav-link" href="fishviewServlet"> 戻る</a>
        <footer>
            <ul>
         <li><a  href="userdetailservlet?id=${userInfo.id}"> ${userInfo.name}さんのユーザ情報</a></li>
            </ul>
        </footer>
    </div>
</body></html>