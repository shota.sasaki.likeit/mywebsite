package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comment;
import beans.Fish;
import dao.CommentDao;
import dao.FishboardDao;

/**
 * Servlet implementation class CommentdeleteServlet
 */
@WebServlet("/CommentdeleteServlet")
public class CommentdeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CommentdeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String commentid =request.getParameter("commentid");
		int commentid2=Integer.parseInt(commentid);

		String boardid =request.getParameter("boardid");
		int boardid2=Integer.parseInt(boardid);

		CommentDao commentdao=new CommentDao();
		commentdao.commentdelete(commentid2);

		FishboardDao Fishboarddao=new FishboardDao();



		try {
			Fish fishboarddetail=Fishboarddao.fishboarddetail(boardid2);
			request.setAttribute("fishboarddetail", fishboarddetail);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}


		try {
			ArrayList<Comment> commentList=commentdao.commentList(boardid2);
			request.setAttribute("commentList", commentList);


		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/fishboard.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
