<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>title</title>


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
   <link href="css/common.css" rel="stylesheet" type="text/css" />
    </head>

<body>
<div id="wrapper">

 <nav class="navbar  navbar-light " style="background-color: azure";>

             <h1><p class="fishtitle text-primary">熱帯魚の館</p>
         </h1>
        <ul class="l navbar-nav flex-row">
         <li class="nav-item">
                <a class="nav-link" > ${userInfo.name}さん</a>
            </li>
         <li class="nav-item">
              　　  <a class="btn btn-primary" href="LoginServlet">ログアウト</a>
            </li>
        </ul>
    </nav>

    <br>

    <div class="text-center">
        <font size="6">ユーザー情報</font>
    </div>
    <br>
    <br>

        <div class="form-group row">
        <label for="loginId" class="col-sm-2 col-form-label">　　ログインID</label>
        <div class="col-sm-10">
            <p class="form-control-plaintext">${user.loginId}</p>
        </div>
    </div>

    <div class="form-group row">
        <label for="userName" class="col-sm-2 col-form-label">　　ユーザ名</label>
        <div class="col-sm-10">
            <p class="form-control-plaintext">${user.name}</p>
        </div>
    </div>
    <div class="form-group row">
        <label for="birthDate" class="col-sm-2 col-form-label">　　生年月日</label>
        <div class="col-sm-10">
            <p class="form-control-plaintext">${user.birthDate}</p>
        </div>
    </div>

    <div class="form-group row">
        <label for="createDate" class="col-sm-2 col-form-label">　　登録日時</label>
        <div class="col-sm-10">
            <p class="form-control-plaintext">${user.createDate}</p>
        </div>
    </div>

    <div class="form-group row">
        <label for="updateDate" class="col-sm-2 col-form-label">　　更新日時</label>
        <div class="col-sm-10">
            <p class="form-control-plaintext">${user.updateDate}</p>
        </div>
    </div>
    <br>

 <div class="form-group row">
    <a class="btn btn-success col-sm-2" href="UserupdateServlet?id=${userInfo.id}">ユーザー情報更新</a>

   　　 <a class="btn btn-danger col-sm-2" href="UserdeleteServlet?id=${userInfo.id}">ユーザー情報削除</a>
</div>
     <br>
     <br>

      　　<a class="nav-link"  href="fishviewServlet"> 戻る</a>

    <footer>
          <ul><li><a href="fishresister?userid=${userInfo.id}">　　熱帯魚情報登録</a></li></ul>
        </footer>
    </div>
</body></html>
