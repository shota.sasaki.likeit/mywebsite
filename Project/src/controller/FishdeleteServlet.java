package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Fish;
import dao.FishDao;

/**
 * Servlet implementation class FishdeleteServlet
 */
@WebServlet("/FishdeleteServlet")
public class FishdeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public FishdeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String fishid = request.getParameter("fishid");
		int fishid2=Integer.parseInt(fishid);

		FishDao fishDao = new FishDao();
		Fish fishdetail;
		try {
			fishdetail = fishDao.fishdetailfindbyfishid(fishid2);
			request.setAttribute("fishdetail", fishdetail);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/fishdelete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String fishid = request.getParameter("fishid");
		int fishid2=Integer.parseInt(fishid);


		FishDao fishDao=new FishDao();
		try {
			fishDao.fishdelete(fishid2);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		response.sendRedirect("fishviewServlet");
	}

}
