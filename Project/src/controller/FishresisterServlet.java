package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import dao.FishDao;

/**
 * Servlet implementation class fishresisterServlet
 */
@WebServlet("/fishresister")

//この記述は画像データの登録先→WebContent内のimageのところ、プロパティよりロケーションを持ってきた
@MultipartConfig(location="C:\\Users\\LIKEIT_STUDENT.DESKTOP-R210GVO\\Documents\\mywebsite\\Project\\WebContent\\image", maxFileSize=1048576)
public class FishresisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public FishresisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/fishresister.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  request.setCharacterEncoding("UTF-8");

		  String userid = request.getParameter("userid");
		  System.out.println(userid);

		  String fishName=request.getParameter("fishName");
		  String fishhabitat=request.getParameter("fishhabitat");
		  String fishsize=request.getParameter("fishsize");
		  String fishlifespan=request.getParameter("fishlifespan");
		  String fishdetail=request.getParameter("fishdetail");

		  // 画像データを取得
		 Part image =  request.getPart("fishimage");
		 String fishImageName = this.getFileName(image);

			if(fishName.isEmpty()||fishhabitat.isEmpty()||fishsize.isEmpty()||fishlifespan.isEmpty()||fishdetail.isEmpty()||fishImageName.isEmpty())
			 {	request.setAttribute("errMsg", "登録されていない内容があります。");

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/fishresister.jsp");
				dispatcher.forward(request, response);
				return;

			 }

		 image.write(fishImageName);


		  FishDao fishDao=new FishDao();

		  try {
			fishDao.insertfishdata(userid,fishName,fishhabitat,fishsize,fishlifespan,fishdetail,fishImageName);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		  response.sendRedirect("fishviewServlet");
	}

	private String getFileName(Part part) {
        String name = null;
        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
            if (dispotion.trim().startsWith("filename")) {
                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
                name = name.substring(name.lastIndexOf("\\") + 1);
                break;
            }
        }
        return name;
    }
}


