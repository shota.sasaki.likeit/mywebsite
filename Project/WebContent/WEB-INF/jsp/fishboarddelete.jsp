<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>title</title>
       <link href="css/common.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    </head>

<body>
<div id="wrapper">

    <nav class="navbar  navbar-light " style="background-color: azure";>

             <h1><p class="fishtitle text-primary">熱帯魚の館</p>
         </h1>
     <ul class="l navbar-nav flex-row">
         <li class="nav-item">
                <a class="nav-link" > ${userInfo.name}さん</a>
            </li>
         <li class="nav-item">
              　　  <a class="btn btn-primary" href="LoginServlet">ログアウト</a>
            </li>
        </ul>
    </nav>


    <br>
     <br>

    <div class="container">
        <div class="delete-area">
            <p>掲示板:${fishboarddetail.boardname}<br>を本当に削除してもよろしいですか？</p>
            <div class="row">
                <div class="col-sm-6">
                    <a href="FishboardServlet?userid=${userInfo.id}&fishboardid=${fishboarddetail.boardid}" class="btn btn-light btn-block">キャンセル</a>
                </div>
                <div class="col-sm-6">
                  <form  action="FishboarddeleteServlet?fishboardid=${fishboarddetail.boardid}&fishid=${fishboarddetail.fishid}" method="post" > <input class="btn btn-primary btn-block" type="submit" value="OK"></form>
                </div>
            </div>
        </div>
    </div>


 <footer>
            <a href="userresisterServlet"> ユーザ情報新規登録</a>
        </footer>
    </div>
</body>
</html>