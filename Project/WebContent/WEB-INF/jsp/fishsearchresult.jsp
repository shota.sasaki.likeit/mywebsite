<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>title</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
<link href="css/common.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<div id="wrapper">
		<nav class="navbar  navbar-light " style="background-color: azure";>

		<h1>
			<p class="fishtitle text-primary">熱帯魚の館</p>
		</h1>
		<ul class="l navbar-nav flex-row">
			<li class="nav-item"><a class="nav-link"
				href="userdetailservlet?id=${userInfo.id}"> ${userInfo.name}さん</a></li>
			<li class="nav-item"><a class="btn btn-primary"
				href="LoginServlet">ログアウト</a></li>
		</ul>
		</nav>
	<div class="container">
		<p class="fishsearchresult col m3">検索結果</p>


				<div class="row">
					<c:forEach var="fishviewList" items="${fishviewList}"
						varStatus="status">
						<%--statusは画像の枚数--%>
						<div class="col s12 m3">
							<div class="card">
								<div class="card-image">
									<a href="FishdetailServlet?fish_id=${fishviewList.fishid}&page_num=${pageNum}">
									<img src="image/${fishviewList.fishImageName}" class="fishview"></a>
								</div>
								<div class="card-content">
									<span class="card-title">${fishviewList.fishName}</span>
								</div>
							</div>
						</div>
						<c:if test="${(status.index + 1) % 4 == 0}">
				</div>
				<div class="row">
					</c:if>
					</c:forEach>
				</div>
				</div>

				<a class="nav-link" href="fishviewServlet"> 戻る</a>

				<footer> <ul><li><a href="fishresister?userid=${userInfo.id}">
					熱帯魚情報登録</a></li></ul> </footer>
			</div>
</body>
</html>