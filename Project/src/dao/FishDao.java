package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.Fish;

public class FishDao {




	public void insertfishdata(String userid,String fishName,String fishhabitat,String fishsize,String fishlifespan,String fishdetail,String fishImageName) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt=null;






		try {

			conn = DBManager.getConnection();

			String sql=" INSERT INTO fishdata(user_id,fishname,fishhabitat,fishsize,fishlifespan,fishdetail,file_name,resister_date)VALUES (?,?,?,?,?,?,?,NOW())";
			stmt  =  conn.prepareStatement(sql);
			//preparestatementはの書き方しっかり覚える

			stmt.setString(1, userid);
			stmt.setString(2, fishName);
			stmt.setString(3, fishhabitat);
			stmt.setString(4,fishsize);
			stmt.setString(5,fishlifespan);
			stmt.setString(6, fishdetail);
			stmt.setString(7,fishImageName);


			stmt.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}





		}
	}
	}



public ArrayList<Fish> fishview() throws SQLException {
	Connection con = null;
	PreparedStatement st = null;
	try {
		con = DBManager.getConnection();

		st = con.prepareStatement("SELECT fishid,file_name,fishName FROM fishdata ");


		ResultSet rs = st.executeQuery();

		ArrayList<Fish> fishviewList = new ArrayList<Fish>();

		while (rs.next()) {

			int fishid=rs.getInt("fishid");
			String fishname=rs.getString("fishname");
			String fishfilename= rs.getString("file_name");

            Fish fishview=new Fish(fishid,fishname,fishfilename);
            fishviewList.add(fishview);
		}
		System.out.println("getAllItem completed");
		return fishviewList;

	} catch (SQLException e) {
		System.out.println(e.getMessage());
		throw new SQLException(e);
	} finally {
		if (con != null) {
			con.close();
		}
	}
}

public ArrayList<Fish> getfishviewbyfishname(String searchWord) throws SQLException {
	Connection con = null;
	PreparedStatement st = null;
	try {
		con = DBManager.getConnection();
		st = con.prepareStatement("SELECT * FROM fishdata WHERE fishname LIKE ?");
		st.setString(1,"%"+searchWord+"%");

	ResultSet rs = st.executeQuery();
	ArrayList<Fish> fishviewList = new ArrayList<Fish>();

	while(rs.next()) {
		int fishid=rs.getInt("fishid");
		String fishname=rs.getString("fishname");
		String fishfilename=rs.getString("file_name");

		Fish fishview=new Fish(fishid,fishname,fishfilename);
		fishviewList.add(fishview);
	}    return fishviewList;
	} catch (SQLException e) {
		System.out.println(e.getMessage());
		throw new SQLException(e);
	} finally {
		if (con != null) {
			con.close();
		}
	}
}







public Fish fishdetailfindbyfishid(int fishid) throws SQLException {
	// TODO 自動生成されたメソッド・スタブ
	Connection con = null;

	try {
		con = DBManager.getConnection();

		String sql="SELECT * FROM fishdata WHERE fishid=?";

		PreparedStatement pStmt=con.prepareStatement(sql);

		pStmt.setInt(1, fishid);
		ResultSet rs = pStmt.executeQuery();

		if (!rs.next()) {
			return null;
		}

		int userid= rs.getInt("user_id");
		String fishname=rs.getString("fishname");
		String fishhabitat=rs.getString("fishhabitat");
		String fishsize=rs.getString("fishsize");
		String fishlifespan=rs.getString("fishlifespan");
		String fishdetail=rs.getString("fishdetail");
		String fishfilename= rs.getString("file_name");

        Fish Fishdetail=new Fish(fishid,userid,fishname,fishhabitat,fishsize,fishlifespan,fishdetail,fishfilename);





	return Fishdetail;


	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
	// データベース切断
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
}

public void fishdelete(int fishid) throws SQLException {
	Connection conn=null;
	PreparedStatement stmt=null;

	try {
		conn=DBManager.getConnection();
		String sql="DELETE FROM fishdata WHERE fishid =? ";

		stmt = conn.prepareStatement(sql);

		stmt.setInt(1, fishid);

		stmt.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}
}



public void updatefish(String fishImageName,int fishid,String fishName, String fishhabitat,String fishsize, String fishlifespan,String fishdetail) throws SQLException {
	Connection conn = null;
	PreparedStatement stmt=null;

	try {

		// データベースへ接続
		conn = DBManager.getConnection();
		String sql = "UPDATE fishdata SET fishname=?,fishhabitat=?,fishsize=?,fishlifespan=?,fishdetail=?,update_date=NOW() ";
		if(!(fishImageName.equals(""))) {
			sql+=" AND filename='"+fishImageName+"'";
		}
		sql+="WHERE fishid=?";

		stmt  =  conn.prepareStatement(sql);

		stmt.setString(1, fishName);
		stmt.setString(2, fishhabitat);
		stmt.setString(3,fishsize);
		stmt.setString(4,fishlifespan);
		stmt.setString(5,fishdetail);
		stmt.setInt(6,fishid);


		stmt.executeUpdate();


	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
}