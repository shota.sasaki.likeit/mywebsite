package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comment;
import beans.Fish;
import dao.CommentDao;
import dao.FishboardDao;

/**
 * Servlet implementation class FishboardServlet
 */
@WebServlet("/FishboardServlet")
public class FishboardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public FishboardServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 request.setCharacterEncoding("UTF-8");

			String fishboardid =request.getParameter("fishboardid");


			int fishboardid2=Integer.parseInt(fishboardid);


			FishboardDao Fishboarddao=new FishboardDao();



			try {
				Fish fishboarddetail=Fishboarddao.fishboarddetail(fishboardid2);
				request.setAttribute("fishboarddetail", fishboarddetail);

			} catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			CommentDao commentdao=new CommentDao();

			try {
				ArrayList<Comment> commentList=commentdao.commentList(fishboardid2);
				request.setAttribute("commentList", commentList);


			} catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/fishboard.jsp");
			dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
