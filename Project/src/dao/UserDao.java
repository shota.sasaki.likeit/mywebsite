package dao;


import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.bind.DatatypeConverter;

import beans.User;



public class UserDao {


	public String passwordlock(String password) {
		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes=null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		//標準出力
		System.out.println("password"+result);

		return result;
	}


	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {

			String result=passwordlock(password);


			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, result);
			ResultSet rs = pStmt.executeQuery();


			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			String idData = rs.getString("id");
			return new User(loginIdData, nameData,idData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public User findByLoginId(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();


			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			//次にいけない、つまり結果表に何もなければnullを返す
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
		// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void insertuser(String loginId,String password,String passwordConf,String userName,String birthDate ) {
		Connection conn = null;
		PreparedStatement stmt=null;

		try {

			String result=passwordlock(password);


			// データベースへ接続
			conn = DBManager.getConnection();
			String sql = "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) VALUES (?,?,?,?,NOW(),NOW())";
			stmt  =  conn.prepareStatement(sql);

			stmt.setString(1, loginId);
			stmt.setString(2, userName);
			stmt.setString(3,birthDate);
			stmt.setString(4,result);



			stmt.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
}

public User findid(String id) {
	Connection conn = null;


	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備

		String sql = "SELECT * FROM user WHERE id = ? ";

		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, id);
		ResultSet rs = pStmt.executeQuery();

		// 結果表に格納されたレコードの内容を

		if(!rs.next()) {
			return null;
		}


			String uid=id;
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			User user = new User(uid,loginId, name, birthDate, password, createDate, updateDate);
          return user;

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}

}
public User userdetailbyloginId(String loginid) {
	Connection conn = null;


	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備

		String sql = "SELECT * FROM user WHERE login_id = ? ";

		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, loginid);
		ResultSet rs = pStmt.executeQuery();

		if (!rs.next()) {
			return null;
		}

		// 結果表に格納されたレコードの内容を
		// Userインスタンスに設定し、ArrayListインスタンスに追加

			String uid=rs.getString("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");

			//beansに詰めたものを持ってくる
			User user = new User(uid,loginId, name, birthDate, password, createDate, updateDate);

			return user;

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}

		}
	}
}

public void updateuser(String loginId,String password,String passwordConf,String userName,String birthDate ) {
	Connection conn = null;
	PreparedStatement stmt=null;

	try {
		String result=passwordlock(password);




		// データベースへ接続
		conn = DBManager.getConnection();
		String sql = "UPDATE user SET name=?,birth_date=?,password=?,update_date=NOW() WHERE login_id=?";
		stmt  =  conn.prepareStatement(sql);

		stmt.setString(1, userName);
		stmt.setString(2, birthDate);
		stmt.setString(3,result);
		stmt.setString(4,loginId);


		stmt.executeUpdate();


	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
public void userdelete(String id) {
	Connection conn=null;
	PreparedStatement stmt=null;

	try {
		conn=DBManager.getConnection();
		String sql="DELETE FROM user WHERE id =? ";

		stmt = conn.prepareStatement(sql);

		stmt.setString(1, id);

		stmt.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}
}
}


